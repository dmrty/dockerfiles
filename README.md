
#Collection of lightweight and ready-to-use docker images
Clone of [@schickling/dockerfiles](https://www.github.com/schickling/dockerfiles). Thanks to him


## Images

* **[Mailcatcher](https://gitlab.com/dmrty/dockerfiles/tree/master/mailcatcher)** - Extra small mailcatcher image
* **[postgres-backup-s3](https://gitlab.com/dmrty/dockerfiles/tree/master/postgres-backup-s3)** - Backup PostgresSQL to S3 (supports periodic backups)
 **[postgres-restore-s3](https://gitlab.com/dmrty/dockerfiles/tree/master/postgres-restore-s3)** - Restore PostgresSQL from S3

